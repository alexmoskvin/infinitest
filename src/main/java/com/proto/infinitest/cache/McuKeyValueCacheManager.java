package com.proto.infinitest.cache;

import com.proto.infinitest.entities.McuEntity;
import org.infinispan.client.hotrod.RemoteCache;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Provides interface for accessing generic key-value cache that contains MCU entries
 */
@Component
public class McuKeyValueCacheManager extends KeyValueCacheManager<McuEntity> {

    private final RemoteCache<String, McuEntity> mcuCache;

    public McuKeyValueCacheManager(@Qualifier("mcuCache") RemoteCache<String, McuEntity> mcuCache) {
        this.mcuCache = mcuCache;
    }

    @Override
    protected RemoteCache<String, McuEntity> getCache() {
        return mcuCache;
    }
}
