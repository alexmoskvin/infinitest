package com.proto.infinitest.cache;

import com.proto.infinitest.entities.McuEntity;
import org.infinispan.client.hotrod.RemoteCache;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Provides interface for accessing indexed cache that contains MCU entries
 */
@Component
public class McuIndexedCacheManager extends IndexedCacheManager<McuEntity> {

    private final RemoteCache<String, McuEntity> mcuIndexedCache;

    public McuIndexedCacheManager(
            @Qualifier("mcuCacheIndexed") RemoteCache<String, McuEntity> mcuIndexedCache) {
        this.mcuIndexedCache = mcuIndexedCache;
    }

    @Override
    protected RemoteCache<String, McuEntity> getCache() {
        return mcuIndexedCache;
    }
}
