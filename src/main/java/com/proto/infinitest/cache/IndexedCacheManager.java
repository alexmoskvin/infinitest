package com.proto.infinitest.cache;

import com.proto.infinitest.entities.BaseEntity;
import org.infinispan.client.hotrod.Search;
import org.infinispan.query.dsl.QueryFactory;

/**
 * Base class that abstracts access to Infinispan cache buckets
 * by providing access via indexed fields and search queries
 * @param <T>
 */
public abstract class IndexedCacheManager<T extends BaseEntity> extends KeyValueCacheManager<T> {

    public QueryFactory getQueryFactory() {
        return Search.getQueryFactory(getCache());
    }
}
