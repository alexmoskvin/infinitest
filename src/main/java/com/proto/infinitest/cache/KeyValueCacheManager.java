package com.proto.infinitest.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.proto.infinitest.entities.BaseEntity;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.commons.util.CloseableIterator;

/**
 * Base class that abstracts access to Infinispan cache buckets
 * by providing access via key-value approach
 * @param <T>
 */
public abstract class KeyValueCacheManager<T extends BaseEntity> {

    /**
     * Returns RemoteCache instance that identifies cache bucket to apply operations to
     */
    protected abstract RemoteCache<String, T> getCache();

    /**
     * Returns raw Infinispan statistics for a cache bucket provided in {@link #getCache()}
     */
    public Map<String, String> getStats() {
        return getCache().stats().getStatsMap();
    }

    /**
     * Clears cache bucket
     */
    public void clear() {
        getCache().clearAsync();
    }

    /**
     * Inserts list of items to Infinispan's cache bucket
     * @param items items to persist
     */
    public void insert(List<T> items) {
        Map<String, T> objectsToPersist = items.stream()
                .collect(Collectors.toMap(BaseEntity::getKey, i -> i));
        getCache().putAll(objectsToPersist);
    }

    /**
     * Returns single entry from cache bucket by given key
     * @param key object id
     * @return cached object
     */
    public T get(String key) {
        return (T) getCache().get(key);
    }

    /**
     * Returns all records from the cache bucket
     * @param keys keys of object identifiers to lookup
     * @return map where key is object id and value object itself
     */
    public Map<String, T> getAll(Set<String> keys) {
        return (Map<String, T>) getCache().getAll(keys);
    }

    /**
     * Deletes records by given key
     * @param key object identifier
     */
    public void delete(String key) {
        getCache().remove(key);
    }

    /**
     * Find first N records specified by 'top' argument
     * @param top number of first records in case to return
     * @return List of records
     */
    public List<T> findTop(int top) {

        List<T> result = new ArrayList<T>();

        try (CloseableIterator<T> iterator = getCache().values().iterator())
        {
            int i = 0;
            while (true) {
                if (!iterator.hasNext()) {
                    break;
                }

                result.add((T) iterator.next());

                if (++i == top) {
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Replaces 'what' entry identified by 'key' with 'what' object
     * @param key object key
     * @param what record to replace
     * @param with record to replace with
     */
    public void replace(String key, T what, T with) {
        getCache().replace(key, what, with);
    }
}
