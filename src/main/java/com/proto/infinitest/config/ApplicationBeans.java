package com.proto.infinitest.config;

import java.io.IOException;

import com.proto.infinitest.config.marshaller.ManualSerializationContextInitializer;
import com.proto.infinitest.entities.McuEntity;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.configuration.ConfigurationBuilder;
import org.infinispan.commons.marshall.JavaSerializationMarshaller;
import org.infinispan.commons.marshall.ProtoStreamMarshaller;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

@Configuration
public class ApplicationBeans {

    @Value("classpath:lib.proto")
    private Resource libProto;

    /**
     * Builds regular Cache manager that interacts with remote Infinispan cluster
     * Infinispan automatically resolves cluster topology with all peers,
     * and usually it's enough to add only a single node here to the configuration, it will resolve the rest on its own
     * @param properties injected application properties
     * @return RemoteCacheManager instance
     */
    @Bean("genericCacheManager")
    public RemoteCacheManager createGenericCacheManager(ApplicationProperties properties) {

        ConfigurationBuilder builder = createGenericCacheManagerBuilder(properties);

        // by default Infinispan offers Java native serialization
        // and serializable beans should be added to whitelist here
        builder.marshaller(new JavaSerializationMarshaller())
                .addJavaSerialWhiteList(getWhitelistedEntities());

        return new RemoteCacheManager(builder.build());
    }

    /**
     * Builds indexed Cache manager that interacts with remote Infinispan cluster
     * Indexed Cache manager uses Protobuf protocol instead of POJO serialization
     * @param properties injected application properties
     * @return RemoteCacheManager instance
     */
    @Bean("indexedCacheManager")
    public RemoteCacheManager createIndexedCacheManager(ApplicationProperties properties) {
        ConfigurationBuilder builder = createGenericCacheManagerBuilder(properties);

        builder.addContextInitializers(new ManualSerializationContextInitializer(libProto));

        // for indexed cache buckets Infinispan uses protobuf, so we add it here
        builder.marshaller(new ProtoStreamMarshaller());

        return new RemoteCacheManager(builder.build());
    }

    private ConfigurationBuilder createGenericCacheManagerBuilder(ApplicationProperties properties)
    {
        ConfigurationBuilder builder = new ConfigurationBuilder();

        if (properties.getInfinispanServers().isEmpty()) {
            throw new RuntimeException("Infinispan remote servers are not defined, exiting");
        }

        for (String server : properties.getInfinispanServers()) {
            builder.addServer().host(server).port(properties.getInfinispanPort());
        }

        return builder;
    }

    @Bean(name = "mcuCache")
    public RemoteCache<String, McuEntity> getMcuCache(
            @Qualifier("genericCacheManager") RemoteCacheManager cacheManager) {
        return cacheManager.getCache("mcu");
    }

    @Bean(name = "mcuCacheIndexed")
    public RemoteCache<String, McuEntity> getIndexedMcuCache(
            @Qualifier("indexedCacheManager") RemoteCacheManager cacheManager) throws IOException {

        return cacheManager.getCache("mcu_indexed");
    }

    private String[] getWhitelistedEntities() {
        return new String[]{"java.util.List", "java.util.ArrayList", "com.proto.infinitest.entities.*"};
    }

}
