package com.proto.infinitest.config.marshaller;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;

import com.proto.infinitest.entities.protobuf.McuEntityProtobufMarshaller;
import org.infinispan.protostream.FileDescriptorSource;
import org.infinispan.protostream.SerializationContext;
import org.infinispan.protostream.SerializationContextInitializer;
import org.springframework.core.io.Resource;

/**
 * SerializationContextInitializer defines protobuf serialization details
 * It requires us pointing marshaller and .proto file contents which it sends to Infinispan instance
 * so it knows how to deserialize our payloads
 */
public class ManualSerializationContextInitializer implements SerializationContextInitializer {

    private Resource libProtoResource;

    public ManualSerializationContextInitializer(Resource libProto) {
        this.libProtoResource = libProto;
    }

    /**
     * Provides Infinispan serialization infrastucture with proto file name
     */
    @Override
    public String getProtoFileName() {
        return "lib.proto";
    }

    /**
     * Provides Infinispan serialization infrastucture with lib.proto file contents
     * which it sends to Infinispan server
     * @return lib.proto contents
     * @throws UncheckedIOException
     */
    @Override
    public String getProtoFile() throws UncheckedIOException {
        try {
            return new String(Files.readAllBytes(libProtoResource.getFile().toPath()));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void registerSchema(SerializationContext serCtx) {
        serCtx.registerProtoFiles(FileDescriptorSource.fromString(getProtoFileName(), getProtoFile()));
    }

    /**
     * Register protobuf marshallers. Could be multiples if we have multiple entities
     * @param serCtx serialization context
     */
    @Override
    public void registerMarshallers(SerializationContext serCtx) {
        serCtx.registerMarshaller(new McuEntityProtobufMarshaller());
    }
}
