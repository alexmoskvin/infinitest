package com.proto.infinitest.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration properties mapped from application.properties
 */
@Configuration
public class ApplicationProperties {

    @Value("#{'${infinispan.server.addrs}'.split(',')}")
    private List<String> infinispanServers;

    @Value("${infinispan.server.port:11222}")
    private Integer infinispanPort;

    public List<String> getInfinispanServers() {
        return infinispanServers;
    }

    public void setInfinispanServers(List<String> infinispanServers) {
        this.infinispanServers = infinispanServers;
    }

    public Integer getInfinispanPort() {
        return infinispanPort;
    }

    public void setInfinispanPort(Integer infinispanPort) {
        this.infinispanPort = infinispanPort;
    }
}
