package com.proto.infinitest.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Random;

/**
 * Miscellaneous helper functions for handling dates and times
 */
public class DateUtils
{
    public static LocalDateTime convert(Date date) {
        if (date == null) {
            return null;
        }

        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public static Date convert(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }

        Instant instant = dateTime.toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    public static LocalDateTime randomDateTime(int pastYears) {
        LocalDateTime dateTime = LocalDateTime.now()
                .minus(Period.ofDays((new Random().nextInt(365 * pastYears))))
                .minusSeconds(new Random().nextInt(86400));
        return dateTime;
    }
}
