package com.proto.infinitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.proto.infinitest")
public class InfinitestApplication {

    public static void main(String[] args) {
        SpringApplication.run(InfinitestApplication.class, args);
    }

}
