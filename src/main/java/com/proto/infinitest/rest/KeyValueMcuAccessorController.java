package com.proto.infinitest.rest;

import java.util.Arrays;
import java.util.HashSet;

import com.proto.infinitest.rest.dto.GenericResponseDto;
import com.proto.infinitest.rest.dto.MultiMcuIdRequestDto;
import com.proto.infinitest.service.McuKeyValueService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.proto.infinitest.rest.SimpleRequestExecutor.execute;

/**
 * Controller that provides endpoints for accessing cached entities
 * in non-indexed key-value Infinispan bucket with POJO based serialization
 */
@RestController
public class KeyValueMcuAccessorController {

    @Autowired
    private McuKeyValueService mcuKeyValueService;

    @Operation(summary = "Generates given number of random MCU records", tags = {"mcu-key-value"})
    @RequestMapping(value = "/mcu-kv/generate", method = RequestMethod.GET)
    public GenericResponseDto generate(
            @Parameter(description="Number of random records to generate", required=true)
            @RequestParam int size) {
        return execute(() -> mcuKeyValueService.generate(size));
    }

    @Operation(summary = "Finds first given number of MCU records", tags = {"mcu-key-value"})
    @RequestMapping(value = "/mcu-kv/top", method = RequestMethod.GET)
    public GenericResponseDto top(
            @Parameter(description="Number of top records to return", required=true)
            @RequestParam int size) {
        return execute(() -> mcuKeyValueService.findTop(size));
    }

    @Operation(summary = "Finds MCU record by a given key", tags = {"mcu-key-value"})
    @RequestMapping(value = "/mcu-kv/{mcuId}", method = RequestMethod.GET)
    public GenericResponseDto find(
            @Parameter(description="MCU ID to lookup", required=true, example = "S29670775451852167884927233")
            @PathVariable String mcuId) {
        return execute(() -> mcuKeyValueService.find(mcuId));
    }

    @Operation(summary = "Finds all MCU records by a given keys", tags = {"mcu-key-value"})
    @RequestMapping(value = "/mcu-kv/all", method = RequestMethod.POST)
    public GenericResponseDto findAll(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description="MCU IDs to lookup", required=true)
            @RequestBody MultiMcuIdRequestDto mcuIds) {
        return execute(() -> mcuKeyValueService.findAll(new HashSet<>(Arrays.asList(mcuIds.getMcuIds()))));
    }

    @Operation(summary = "Deletes MCU record by a given key", tags = {"mcu-key-value"})
    @RequestMapping(value = "/mcu-kv/delete/{mcuId}", method = RequestMethod.GET)
    public GenericResponseDto delete(
            @Parameter(description="MCU ID to delete", required=true, example = "S13414255918583198649582424")
            @PathVariable String mcuId) {
        return execute(() -> mcuKeyValueService.delete(mcuId));
    }

}
