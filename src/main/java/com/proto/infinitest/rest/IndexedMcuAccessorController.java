package com.proto.infinitest.rest;

import java.time.LocalDateTime;

import com.proto.infinitest.rest.dto.GenericResponseDto;
import com.proto.infinitest.service.McuIndexedService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.proto.infinitest.rest.SimpleRequestExecutor.execute;

/**
 * Controller that provides endpoints for accessing cached entities
 * in indexed Infinispan bucket with protobuf based serialization
 */
@RestController
public class IndexedMcuAccessorController {

    @Autowired
    private McuIndexedService mcuIndexedService;

    @Operation(summary = "Finds MCU records after specified timestamp date", tags = {"mcu-index"})
    @RequestMapping(value = "/mcu-idx/after", method = RequestMethod.GET)
    public GenericResponseDto after(
            @Parameter(description="Date to lookup records", required=true, example = "2019-01-07T01:30:00")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime afterDate) {
        return execute(() -> mcuIndexedService.findByTimestamp(afterDate));
    }

    @Operation(summary = "Finds MCU records after specified timestamp date", tags = {"mcu-index"})
    @RequestMapping(value = "/mcu-idx/afterWithSystemType", method = RequestMethod.GET)
    public GenericResponseDto afterWithSystemType(
            @Parameter(description="Date to lookup records", required=true, example = "2019-01-07T01:30:00")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime afterDate,
            @Parameter(description="System type to filter on", required=true, example = "2")
            @RequestParam Integer systemType) {
        return execute(() -> mcuIndexedService.findByTimestampAndSystemType(afterDate, systemType));
    }

    @Operation(summary = "Generates given number of random MCU records", tags = {"mcu-index"})
    @RequestMapping(value = "/mcu-idx/generate", method = RequestMethod.GET)
    public GenericResponseDto generate(
            @Parameter(description="Number of random records to generate", required=true)
            @RequestParam int size) {
        return execute(() -> mcuIndexedService.generate(size));
    }

    @Operation(summary = "Finds MCU record by a given key", tags = {"mcu-index"})
    @RequestMapping(value = "/mcu-idx/{mcuId}", method = RequestMethod.GET)
    public GenericResponseDto find(
            @Parameter(description="MCU ID to lookup", required=true, example = "S29670775451852167884927233")
            @PathVariable String mcuId) {
        return execute(() -> mcuIndexedService.find(mcuId));
    }

    @Operation(summary = "Finds MCU records by a MCU master ID", tags = {"mcu-index"})
    @RequestMapping(value = "/mcu-idx/master/{mcuId}", method = RequestMethod.GET)
    public GenericResponseDto findByMaster(
            @Parameter(description="MCU master ID to lookup", required=true, example = "S29670775451852167884927233")
            @PathVariable String mcuId) {
        return execute(() -> mcuIndexedService.findByMasterId(mcuId));
    }

    @Operation(summary = "Deletes MCU record by a given key", tags = {"mcu-index"})
    @RequestMapping(value = "/mcu-idx/delete/{mcuId}", method = RequestMethod.GET)
    public GenericResponseDto delete(
            @Parameter(description="MCU ID to delete", required=true, example = "S13414255918583198649582424")
            @PathVariable String mcuId) {
        return execute(() -> mcuIndexedService.delete(mcuId));
    }

}
