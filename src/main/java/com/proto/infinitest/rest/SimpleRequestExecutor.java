package com.proto.infinitest.rest;

import java.util.concurrent.Callable;

import com.proto.infinitest.rest.dto.GenericResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Request executor that wraps request execution and measures time it took to process it
 */
public class SimpleRequestExecutor {

    private static Logger logger = LoggerFactory.getLogger(SimpleRequestExecutor.class);

    public static GenericResponseDto execute(Callable<?> c) {
        return executeImpl(c);
    }

    public static GenericResponseDto execute(Runnable r) {
        return executeImpl(() -> {
            r.run();
            return null;
        });
    }

    private static GenericResponseDto executeImpl(Callable<?> c) {
        GenericResponseDto response = new GenericResponseDto();

        long startTime = System.currentTimeMillis(), duration = 0;

        try {
            response.setData(c.call());
            duration = System.currentTimeMillis() - startTime;
            response.setDuration(duration);
        } catch (Exception e) {
            response.setError(e.getMessage());
        } finally {
            logger.info("Request succeeded in {} msec", duration);
        }

        return response;
    }
}
