package com.proto.infinitest.rest.dto;

/**
 * Describes response sent by REST API
 */
public class GenericResponseDto {

    /**
     * Request duration
     */
    private long duration;

    /**
     * Backend response
     */
    private Object data;

    /**
     * {@link Exception#getMessage()} in case exception occurs
     */
    private String error;

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
