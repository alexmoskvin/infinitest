package com.proto.infinitest.rest.dto;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * Dto used for looking up MCU records by given multiple MCU ids
 */
public class MultiMcuIdRequestDto {

    @ArraySchema(schema = @Schema(description = "Array of unique MCU identifiers",
            example = "S45345345234", type = "array", required = true), minItems = 1)
    private String[] mcuIds;

    public String[] getMcuIds() {
        return mcuIds;
    }

    public void setMcuIds(String[] mcuIds) {
        this.mcuIds = mcuIds;
    }
}
