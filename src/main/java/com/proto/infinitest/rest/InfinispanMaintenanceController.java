package com.proto.infinitest.rest;

import com.proto.infinitest.cache.McuIndexedCacheManager;
import com.proto.infinitest.cache.McuKeyValueCacheManager;
import com.proto.infinitest.rest.dto.GenericResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.proto.infinitest.rest.SimpleRequestExecutor.*;

/**
 * Controller that provides generic Infinispan bucket functionality
 * such as statistics retrieval or bucket flush
 */
@RestController
public class InfinispanMaintenanceController
{
    @Autowired
    private McuKeyValueCacheManager mcuCacheManager;

    @Autowired
    private McuIndexedCacheManager mcuIndexedCacheManager;

    @Operation(summary = "Gets MCU cache bucket stats", tags = {"infinispan" })
    @RequestMapping(value = "/infinispan/mcu/stats", method = RequestMethod.GET)
    public GenericResponseDto mcuCacheStats() {
        return execute(() -> mcuCacheManager.getStats());
    }

    @Operation(summary = "Gets indexed MCU cache bucket stats", tags = {"infinispan" })
    @RequestMapping(value = "/infinispan/mcu_indexed/stats", method = RequestMethod.GET)
    public GenericResponseDto mcuIndexedCacheStats() {
        return execute(() -> mcuIndexedCacheManager.getStats());
    }

    @Operation(summary = "Clears MCU cache bucket", tags = {"infinispan" })
    @RequestMapping(value = "/infinispan/mcu/clear", method = RequestMethod.GET)
    public GenericResponseDto clearMcuCache() {
        return execute(() -> mcuCacheManager.clear());
    }

    @Operation(summary = "Clears indexed MCU cache bucket", tags = {"infinispan" })
    @RequestMapping(value = "/infinispan/mcu_indexed/clear", method = RequestMethod.GET)
    public GenericResponseDto clearIndexedMcuCache() {
        return execute(() -> mcuIndexedCacheManager.clear());
    }
}
