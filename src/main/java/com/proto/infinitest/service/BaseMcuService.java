package com.proto.infinitest.service;

import java.util.Arrays;

import com.proto.infinitest.cache.KeyValueCacheManager;
import com.proto.infinitest.entities.McuEntity;
import com.proto.infinitest.entities.generators.EntityGenerator;
import com.proto.infinitest.entities.generators.McuGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Common logic for both key-value and indexed MCU cache accessors implementation
 */
public abstract class BaseMcuService {

    /**
     * Max amount of records to insert in 1 batch
     */
    private final static int INSERT_BATCH_SIZE = 1000;
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Cache manager accessor method that concrete implementation has to provide
     */
    public abstract KeyValueCacheManager<McuEntity> getCacheManager();

    /**
     * Generate random records which inserts in batches of {@link BaseMcuService#INSERT_BATCH_SIZE} elements
     * @param size amount of records to generate
     */
    public void generate(int size) {

        try {
            int batches = size / INSERT_BATCH_SIZE,
                    remainder = size % INSERT_BATCH_SIZE;

            for (int i = 0 ; i < batches ; i++) {
                getCacheManager().insert(getEntityGenerator().generate(INSERT_BATCH_SIZE));
            }

            getCacheManager().insert(getEntityGenerator().generate(remainder));
        } catch (Exception e) {
            logger.error("Failed generating random {} MCU records", size, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Replaces existing record in a cache bucket
     * @param subject object to replace with
     */
    public void replace(McuEntity subject) {
        try {
            McuEntity existingEntity = getCacheManager().get(subject.getKey());
            if (existingEntity == null) {
                getCacheManager().insert(Arrays.asList(subject));
                return;
            }

            getCacheManager().replace(subject.getKey(), existingEntity, subject);
        } catch (Exception e) {
            logger.error("Failed replacing {} with object {}", subject.getKey(), subject, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Finds cached object by id
     * @param mcuId mcu id to lookup by
     * @return McuEntity instance
     */
    public McuEntity find(String mcuId) {
        try {
            return getCacheManager().get(mcuId);
        } catch (Exception e) {
            logger.error("Failed finding MCU {}", mcuId, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Deletes cached object by id
     * @param mcuId mcu id to lookup by
     */
    public void delete(String mcuId) {
        try {
            getCacheManager().delete(mcuId);
        } catch (Exception e) {
            logger.error("Failed MCU {} removal", mcuId, e);
            throw new RuntimeException(e);
        }
    }

    protected EntityGenerator<McuEntity> getEntityGenerator() {
        return new McuGenerator();
    }
}
