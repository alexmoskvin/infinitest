package com.proto.infinitest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.proto.infinitest.cache.KeyValueCacheManager;
import com.proto.infinitest.cache.McuKeyValueCacheManager;
import com.proto.infinitest.entities.McuEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * High-level service methods that interact with key-value cached MCU buckets
 * with POJO based serialization
 */
@Service
public class McuKeyValueService extends BaseMcuService {

    private KeyValueCacheManager<McuEntity> cacheManager;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public McuKeyValueService(McuKeyValueCacheManager mcuCacheManager) {
        this.cacheManager = mcuCacheManager;
    }

    /**
     * Fetches first N records from the cache
     * @param topRecords number of records to fetch from the cachenbucket
     * @return List of extracted {@link McuEntity} objects
     */
    public List<McuEntity> findTop(int topRecords) {
        try {
            return getCacheManager().findTop(topRecords);
        } catch (Exception e) {
            logger.error("Failed fetching first {} MCU records", topRecords, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Fetches records by given set of their unique identifiers
     * @param mcuIds set of object keys to lookup
     * @return List of extracted {@link McuEntity} objects
     */
    public List<McuEntity> findAll(Set<String> mcuIds) {
        try {
            Map<String, McuEntity> mcuEntries = getCacheManager().getAll(mcuIds);
            return new ArrayList<>(mcuEntries.values());
        } catch (Exception e) {
            logger.error("Failed fetching MCU records {}", mcuIds, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public KeyValueCacheManager<McuEntity> getCacheManager() {
        return cacheManager;
    }
}
