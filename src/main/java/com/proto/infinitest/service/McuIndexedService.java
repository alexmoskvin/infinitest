package com.proto.infinitest.service;

import java.time.LocalDateTime;
import java.util.List;

import com.proto.infinitest.cache.IndexedCacheManager;
import com.proto.infinitest.entities.McuEntity;
import org.infinispan.query.dsl.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.proto.infinitest.utils.DateUtils.convert;

/**
 * High-level service methods that interact with indexed cached MCU buckets
 * with protobuf based serialization
 */
@Service
public class McuIndexedService extends BaseMcuService {

    private IndexedCacheManager<McuEntity> cacheManager;
    private Logger logger = LoggerFactory.getLogger(getClass());

    public McuIndexedService(IndexedCacheManager<McuEntity> cacheManager) {
        this.cacheManager = cacheManager;
    }

    /**
     * Finds all MCU records with given {@link McuEntity#getMasterId()}
     * @param masterId master ID to lookup by
     * @return List of extracted {@link McuEntity} objects that are matching given criteria
     */
    public List<McuEntity> findByMasterId(String masterId) {
        try {
            Query masterIdQuery = cacheManager.getQueryFactory()
                    .from(McuEntity.class)
                    .having("masterId").eq(masterId)
                    .build();
            return masterIdQuery.list();
        } catch (Exception e) {
            logger.error("Failed finding records by MCU master ID {}", masterId, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Finds all MCU records which {@link McuEntity#getTimestamp()} is greater than given instant
     * @param date given instant
     * @return List of extracted {@link McuEntity} objects that are matching given criteria
     */
    public List<McuEntity> findByTimestamp(LocalDateTime date) {
        try {
            Query timeQuery = cacheManager.getQueryFactory()
                    .from(McuEntity.class)
                    .having("timestamp").gt(convert(date))
                    .build();

            return timeQuery.list();
        } catch (Exception e) {
            logger.error("Failed finding records greater than date {}", date, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Finds all MCU records which {@link McuEntity#getTimestamp()} is greater than given instant
     * and {@link McuEntity#getSystemType()} that is equal to given system type
     * @param date given instant
     * @param systemType given system type
     * @return List of extracted {@link McuEntity} objects that are matching given criteria
     */
    public List<McuEntity> findByTimestampAndSystemType(LocalDateTime date, Integer systemType) {
        try {
            Query timeQuery = cacheManager.getQueryFactory()
                    .from(McuEntity.class)
                    .having("timestamp").gt(convert(date))
                    .and()
                    .having("systemType").eq(systemType)
                    .build();

            return timeQuery.list();
        } catch (Exception e) {
            logger.error("Failed finding records greater than date {}", date, e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public IndexedCacheManager<McuEntity> getCacheManager() {
        return cacheManager;
    }
}
