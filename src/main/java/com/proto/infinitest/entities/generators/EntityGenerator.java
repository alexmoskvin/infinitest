package com.proto.infinitest.entities.generators;

import java.util.ArrayList;
import java.util.List;

import com.proto.infinitest.entities.BaseEntity;

/**
 * Defines a contract for a implementation that generates instances of {@link BaseEntity}
 * @param <T>
 */
public interface EntityGenerator<T extends BaseEntity> {

    /**
     * Generates multiple random records
     * @param size amount of records to generate
     * @return list of generated records
     */
    default List<T> generate(int size) {
        List<T> result = new ArrayList<>(size);
        for (int i = 0 ; i < size ; i++) {
            result.add(generate());
        }
        return result;
    }

    /**
     * Generates single random MCU record
     * @return
     */
    T generate();
}
