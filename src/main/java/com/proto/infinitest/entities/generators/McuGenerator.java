package com.proto.infinitest.entities.generators;

import java.time.LocalDateTime;
import java.util.Random;

import com.proto.infinitest.entities.McuEntity;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import static com.proto.infinitest.utils.DateUtils.randomDateTime;

/**
 * Generates random MCU record entries
 */
public class McuGenerator implements EntityGenerator<McuEntity> {

    private Random random = new Random();
    private Integer[] allowedSystemTypes = {0, 2, 4, 9, 15 };
    private Integer[] allowedFwVersions = {717, 718, 716, 707, 3719, 667, 695, 711, 661, 715, 688, 700, 715, 671, 3717};
    private Integer[] allowedGuiVersions = {937, 930, 938, 919, 928, 932, 918, 936, 921};
    private Integer[] allowedHwVer = {1, 2};
    private Integer[] allowedMiscMask = {0, 32, 36, 8, 56, 40, 16, 1};
    private Integer[] allowedVoltageOffset = {0, 99};

    @Override
    public McuEntity generate() {
        McuEntity mcu = new McuEntity();
        mcu.setMcuId(generateId());
        mcu.setSystemType(generateAllowedValue(allowedSystemTypes));
        mcu.setFwVersion(generateAllowedValue(allowedFwVersions));
        mcu.setGuiVersion(generateAllowedValue(allowedGuiVersions));
        mcu.setTimestamp(generateDateTime());
        mcu.setIpAddress(generateIpAddress());
        mcu.setMasterId(generateMasterId());
        mcu.setDaysSinceMaintenance(generateDaysSinceMaintenance());
        mcu.setSpecialCharges(generateSpecialChanges());
        mcu.setHumidityAlarm(generateHumidityAlarm());
        mcu.setHumidityAlarmDate(generateDateTimeOrNull());
        mcu.setWarrantyloss(0);
        mcu.setWarrantylossDate(generateDateTime());
        mcu.setHwVer(generateAllowedValue(allowedHwVer));
        mcu.setMiscMask(generateAllowedValue(allowedMiscMask));
        mcu.setPbLowVoltageOffset(generateAllowedValue(allowedVoltageOffset));
        mcu.setDischargeDepthAdjustment(0);
        return mcu;
    }

    /**
     * Generates MCU ID
     * @return
     */
    private String generateId() {
        return "S" + RandomStringUtils.randomNumeric(26);
    }

    /**
     * Generates random date within last 5 years
     * @return
     */
    private LocalDateTime generateDateTime() {
        return randomDateTime(5);
    }

    /**
     * Generates random date or null
     * @return
     */
    private LocalDateTime generateDateTimeOrNull() {
        if (random.nextInt(2) == 1) {
            return null;
        }

        return generateDateTime();
    }

    /**
     * Generates master MCU ID
     * @return
     */
    private String generateMasterId() {
        int r = random.nextInt(6);
        switch (r) {
            case 0:
            case 1:
            case 2:
                return "S0";
            case 3:
            case 4:
                return generateId();
            default:
                return "Serial_Not_Received";
        }
    }

    private Integer generateDaysSinceMaintenance() {
        return random.nextInt(300);
    }

    private Integer generateSpecialChanges() {
        return random.nextInt(300);
    }

    private Integer generateHumidityAlarm() {
        return random.nextInt(2);
    }

    private Integer generateAllowedValue(Integer[] array) {
        return array[RandomUtils.nextInt(0, array.length-1)];
    }

    private String generateIpAddress() {
        return generateIpOctet() + "." + generateIpOctet() + "." + generateIpOctet() + "." + generateIpOctet();
    }

    private int generateIpOctet() {
        return random.nextInt((255 - 1) + 1) + 1;
    }

}
