package com.proto.infinitest.entities.protobuf;

import java.io.IOException;

import com.proto.infinitest.entities.McuEntity;
import org.infinispan.protostream.MessageMarshaller;

import static com.proto.infinitest.utils.DateUtils.convert;

/**
 * Protobuf marshaller for {@link McuEntity}
 */
public class McuEntityProtobufMarshaller implements MessageMarshaller<McuEntity>
{
    @Override
    public McuEntity readFrom(ProtoStreamReader reader) throws IOException {
        McuEntity entity = new McuEntity();
        entity.setMcuId(reader.readString("mcuId"));
        entity.setSystemType(reader.readInt("systemType"));
        entity.setFwVersion(reader.readInt("fwVersion"));
        entity.setGuiVersion(reader.readInt("guiVersion"));
        entity.setTimestamp(convert(reader.readDate("timestamp")));
        entity.setIpAddress(reader.readString("ipAddress"));
        entity.setMasterId(reader.readString("masterId"));
        entity.setDaysSinceMaintenance(reader.readInt("daysSinceMaintenance"));
        entity.setSpecialCharges(reader.readInt("specialCharges"));
        entity.setHumidityAlarm(reader.readInt("humidityAlarm"));
        entity.setHumidityAlarmDate(convert(reader.readDate("humidityAlarmDate")));
        entity.setWarrantyloss(reader.readInt("warrantyloss"));
        entity.setWarrantylossDate(convert(reader.readDate("warrantylossDate")));
        entity.setHwVer(reader.readInt("hwVer"));
        entity.setMiscMask(reader.readInt("miscMask"));
        entity.setPbLowVoltageOffset(reader.readInt("pbLowVoltageOffset"));
        entity.setDischargeDepthAdjustment(reader.readInt("dischargeDepthAdjustment"));
        return entity;
    }

    @Override
    public void writeTo(ProtoStreamWriter writer, McuEntity mcuEntity) throws IOException {
        writer.writeString("mcuId", mcuEntity.getMcuId());
        writer.writeInt("systemType", mcuEntity.getSystemType());
        writer.writeInt("fwVersion", mcuEntity.getFwVersion());
        writer.writeInt("guiVersion", mcuEntity.getGuiVersion());
        writer.writeDate("timestamp", convert(mcuEntity.getTimestamp()));
        writer.writeString("ipAddress", mcuEntity.getIpAddress());
        writer.writeString("masterId", mcuEntity.getMasterId());
        writer.writeInt("daysSinceMaintenance", mcuEntity.getDaysSinceMaintenance());
        writer.writeInt("specialCharges", mcuEntity.getSpecialCharges());
        writer.writeInt("humidityAlarm", mcuEntity.getHumidityAlarm());
        writer.writeDate("humidityAlarmDate", convert(mcuEntity.getHumidityAlarmDate()));
        writer.writeInt("warrantyloss", mcuEntity.getWarrantyloss());
        writer.writeDate("warrantylossDate", convert(mcuEntity.getWarrantylossDate()));
        writer.writeInt("hwVer", mcuEntity.getHwVer());
        writer.writeInt("miscMask", mcuEntity.getMiscMask());
        writer.writeInt("pbLowVoltageOffset", mcuEntity.getPbLowVoltageOffset());
        writer.writeInt("dischargeDepthAdjustment", mcuEntity.getDischargeDepthAdjustment());
    }

    @Override
    public Class<? extends McuEntity> getJavaClass() {
        return McuEntity.class;
    }

    @Override
    public String getTypeName() {
        return "com.proto.infinitest.entities.McuEntity";
    }
}
