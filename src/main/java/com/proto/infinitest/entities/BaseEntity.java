package com.proto.infinitest.entities;

import java.beans.Transient;
import java.io.Serializable;

/**
 * Base class to be implemented by all entities that are persisted into Infinispan cache
 * Implements {@link Serializable} in order to make POJO serialization work for simple key-value lookups
 */
public abstract class BaseEntity implements Serializable {

    @Transient
    public abstract String getKey();
}
