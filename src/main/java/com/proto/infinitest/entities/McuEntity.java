package com.proto.infinitest.entities;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.search.annotations.*;

/**
 * Describes MCU record
 */
@Indexed
public class McuEntity extends BaseEntity {

    /**
     * Needed for POJO serialization
     */
    public static final long serialVersionUID = 1L;

    private String mcuId;
    @Field private Integer systemType;
    private Integer fwVersion;
    private Integer guiVersion;
    @Field private LocalDateTime timestamp;
    private String ipAddress;
    @Field private String masterId;
    @Field private Integer daysSinceMaintenance;
    private Integer specialCharges;
    private Integer humidityAlarm;
    private LocalDateTime humidityAlarmDate;
    private Integer warrantyloss;
    private LocalDateTime warrantylossDate;
    @Field private Integer hwVer;
    private Integer miscMask;
    private Integer pbLowVoltageOffset;
    private Integer dischargeDepthAdjustment;

    public String getMcuId() {
        return mcuId;
    }

    public void setMcuId(String mcuId) {
        this.mcuId = mcuId;
    }

    public Integer getSystemType() {
        return systemType;
    }

    public void setSystemType(Integer systemType) {
        this.systemType = systemType;
    }

    public Integer getFwVersion() {
        return fwVersion;
    }

    public void setFwVersion(Integer fwVersion) {
        this.fwVersion = fwVersion;
    }

    public Integer getGuiVersion() {
        return guiVersion;
    }

    public void setGuiVersion(Integer guiVersion) {
        this.guiVersion = guiVersion;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public Integer getDaysSinceMaintenance() {
        return daysSinceMaintenance;
    }

    public void setDaysSinceMaintenance(Integer daysSinceMaintenance) {
        this.daysSinceMaintenance = daysSinceMaintenance;
    }

    public Integer getSpecialCharges() {
        return specialCharges;
    }

    public void setSpecialCharges(Integer specialCharges) {
        this.specialCharges = specialCharges;
    }

    public Integer getHumidityAlarm() {
        return humidityAlarm;
    }

    public void setHumidityAlarm(Integer humidityAlarm) {
        this.humidityAlarm = humidityAlarm;
    }

    public LocalDateTime getHumidityAlarmDate() {
        return humidityAlarmDate;
    }

    public void setHumidityAlarmDate(LocalDateTime humidityAlarmDate) {
        this.humidityAlarmDate = humidityAlarmDate;
    }

    public Integer getWarrantyloss() {
        return warrantyloss;
    }

    public void setWarrantyloss(Integer warrantyloss) {
        this.warrantyloss = warrantyloss;
    }

    public LocalDateTime getWarrantylossDate() {
        return warrantylossDate;
    }

    public void setWarrantylossDate(LocalDateTime warrantylossDate) {
        this.warrantylossDate = warrantylossDate;
    }

    public Integer getHwVer() {
        return hwVer;
    }

    public void setHwVer(Integer hwVer) {
        this.hwVer = hwVer;
    }

    public Integer getMiscMask() {
        return miscMask;
    }

    public void setMiscMask(Integer miscMask) {
        this.miscMask = miscMask;
    }

    public Integer getPbLowVoltageOffset() {
        return pbLowVoltageOffset;
    }

    public void setPbLowVoltageOffset(Integer pbLowVoltageOffset) {
        this.pbLowVoltageOffset = pbLowVoltageOffset;
    }

    public Integer getDischargeDepthAdjustment() {
        return dischargeDepthAdjustment;
    }

    public void setDischargeDepthAdjustment(Integer dischargeDepthAdjustment) {
        this.dischargeDepthAdjustment = dischargeDepthAdjustment;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("mcuId", mcuId)
                .append("systemType", systemType)
                .append("fwVersion", fwVersion)
                .append("guiVersion", guiVersion)
                .append("timestamp", timestamp)
                .append("ipAddress", ipAddress)
                .append("masterId", masterId)
                .append("daysSinceMaintenance", daysSinceMaintenance)
                .append("specialCharges", specialCharges)
                .append("humidityAlarm", humidityAlarm)
                .append("humidityAlarmDate", humidityAlarmDate)
                .append("warrantyloss", warrantyloss)
                .append("warrantylossDate", warrantylossDate)
                .append("hwVer", hwVer)
                .append("miscMask", miscMask)
                .append("pbLowVoltageOffset", pbLowVoltageOffset)
                .append("dischargeDepthAdjustment", dischargeDepthAdjustment)
                .toString();
    }

    @Override
    public String getKey() {
        return mcuId;
    }
}
