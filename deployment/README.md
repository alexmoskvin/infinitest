# Infinispan deployment instructions
  
1\. Clone the repository to each node you want to join to the cluster.
2\. cd into the cloned folder (on each node)
3\. Adjust jgroups.xml file (on each node).

Specifically following snippet needs to be adjusted

```
<TCPPING async_discovery="true" initial_hosts="${jgroups.tcpping.initial_hosts:host1[7800],host2[7800],host3[7800]}" port_range="1"/>
```

There is an `initial_hosts` parameter you have to change by replacing `host1,host2,host3` with IP addresses of servers that form the cluster.

4\. Make deploy.sh executable 
`# chmod 755 ./deploy.sh`

5\. Run deploy.sh script
`# ./deploy.sh`

6\. Adjust RAM utilization in `/opt/infinispan/bin/server.conf`
On line 49, set `-Xmx`, `-Xms`, `-XX:MetaspaceSize` and `-XX:MaxMetaspaceSize` parameters.

Following parameters are for production deployment scenario taking into account that node resources will solely go to caching purposes and there will be no other RAM demanding processes colocated. 

| Parameter  | Recommended value | Description |
| ------------- | ------------- | ------------- |
| `-Xms`  | `-Xms2400M` | This parameter defines heap allocated to a Java process immediately after start. Recommended to be set to 30% of RAM available (2.4G for 8Gb RAM node) | 
| `-Xmx`  | `-Xmx7300M`  | 90% of RAM available (7.3G for 8Gb RAM node)|
| `-XX:MetaspaceSize`  | `-XX:MetaspaceSize=96M` | Initial metaspace size|
| `-XX:MaxMetaspaceSize`  | `-XX:MaxMetaspaceSize=256M` | Max metaspace size. Controls how often GC is triggered. |

So that it looks similarly to below:

```
if [ "x$JAVA_OPTS" = "x" ]; then
 49    JAVA_OPTS="-Xms512M -Xmx6G -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256M -Djava.net.preferIPv4Stack=    true"
 50    JAVA_OPTS="$JAVA_OPTS -Djava.awt.headless=true"
 51 else
 52    echo "JAVA_OPTS already set in environment; overriding default settings with values: $JAVA_OPTS"
 53 fi
```

7\. Restart Infinispan
`# systemctl restart infinispan-server`


