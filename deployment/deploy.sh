#!/bin/bash

set -e

# detecting private and public IP address of the system
PUBLIC_IP=$(hostname -I | awk '{ print $1}')
PRIVATE_IP=$(hostname -I | awk '{ print $2}')

# installing dependencies
yum install -y wget curl unzip net-tools

# installing JDK 11
rpm --import https://yum.corretto.aws/corretto.key
curl -L -o /etc/yum.repos.d/corretto.repo https://yum.corretto.aws/corretto.repo
yum -y update
yum install -y java-11-amazon-corretto-devel

# getting latest stable infinispan and installing it to /opt/
wget https://downloads.jboss.org/infinispan/10.1.3.Final/infinispan-server-10.1.3.Final.zip
unzip infinispan-server-10.1.3.Final.zip
mv infinispan-server-10.1.3.Final /opt/infinispan

# Copying configuration files
# Note: please check jgroups.xml and make sure to substitute following line
# initial_hosts="${jgroups.tcpping.initial_hosts:host1[7800],host2[7800],host3[7800]}"
# with the one that contains real hostnames or IP addresses of the nodes which are a part of a cluster
# this needs to be done before this script gets executed
cp -f ./infinispan.xml /opt/infinispan/server/conf/
cp -f ./jgroups.xml /opt/infinispan/server/conf/

# setting up systemd script for managing infinispan
cp -f ./infinispan-server.service /etc/systemd/system/

# Execute this command for the node which needs to have Web UI exposed
sed -i "s/127.0.0.1/$PUBLIC_IP/g" /opt/infinispan/server/conf/infinispan.xml

# starting services
systemctl daemon-reload
systemctl enable infinispan-server
systemctl start infinispan-server
systemctl status infinispan-server
